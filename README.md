## Kuntur Framework

#### This file was created by Enyert Vinas aka:PPT-ARCHITECT

### Definicion
Kuntur Framework es un marco de trabajo que pertence a Pacifico Seguros y permite simplificar el desarrollo de microservicios
sobre una arquitectura digital.

### Descripcion

Kuntur framework contiene diversos modulos, cada uno de ellos con una funcion bastante especifica, de esta forma se
puede modificar de una forma sencilla. Entre los modulos incluidos podemos citar:

- kuntur-core: Este modulo se encarga de proveer las diversas funcionalidades a cada microservicio que se construya
dentro del framework de kuntur
- kuntur-common-dependencies: Contiene todas las dependencias comunes entre los distintos tipos de microservicio
- kuntur-bff-dependencies: Contiene dependencias particulares para los microservicios de tipo backend for frontend
- kuntur-business-dependencies: Contiene dependencias particulares para los microservicios de tipo business
- kuntur-support-dependencies: Contiene dependencias particulares para los microservicios de tipo support
- kuntur-bff-parent: Proyecto parent para los microservicios de tipo bff
- kuntur-business-parent: Proyecto parent para los microservicios de tipo business
- kuntur-support-parent: Proyecto parent para los microservicios de tipo support